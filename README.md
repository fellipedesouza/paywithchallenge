# PayWith Challenge

This application is about the 3 button sequence challenge.

The applied architecture is MVVM, which is divided with the following layers:

* **Data:** Model classes and enumerators;
* **Domain:** Anything related to the app core logic. Random generators, validators, extensions, dependency injection;
* **Presenter:** Activity and ViewModel, the app UI;

The domain layer is covered by unit tests.

Frameworks adopted:
* **Koin** for dependency injection;
* **JUnit** for unit testing;
* **DataBinding** to bind views with data easily, in order to avoid boilerplate code;

The minimum SDK is 19 (Android 4.4.2, KitKat version).
The app was developed in Android Studio 4.1.

## Building the app

First of all, clone the repository:
`git clone https://fellipedesouza@bitbucket.org/fellipedesouza/paywithchallenge.git`

### Android Studio (IDE)

* Open Android Studio and select `File->Open...`
* Select the project directory cloned in your computer
* Click 'OK' to open the the project in Android Studio.
* A Gradle sync should start, but you can force a sync and build the 'app' module as needed.

### Gradle (command line)

* Build the APK: `./gradlew build`

## Running the Sample App

Connect an Android device to your development machine, or use an Android Virtual Device for the testing purpose.

### Android Studio
* Select `Run -> Run 'app'`
* Select the device you wish to run the app on and click 'OK'

### Gradle
* Install the debug APK on your device `./gradlew installDebug`
* Start the APK: `<path to Android SDK>/platform-tools/adb -d shell am start com.fellipe.paywithchallenge/com.fellipe.paywithchallenge.presenter.main.activity.MainActivity`

## Using the Sample App

Every time the app starts in, a new sequence is randomly generated. The objective is to guess the correct clicking sequence.

The third LED will always represent the last click, and the others represent the previous ones. If the LED becomes green, you clicked the right button in the right position, so you can continue. If the LED is orange, the button exists in the sequence, but not at this position. If the LED is red, this button should not be clicked in this sequence.

Everytime the LED is green, you can keep trying, otherwise, the sequence is restarted and you should try again. If you can color the 3 LEDs with the green color, that means you won, so you can retry with a new sequence.

