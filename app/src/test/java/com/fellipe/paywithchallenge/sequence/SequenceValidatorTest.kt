package com.fellipe.paywithchallenge.sequence

import com.fellipe.paywithchallenge.data.led.LEDStatus
import com.fellipe.paywithchallenge.data.model.sequence.ButtonValue
import com.fellipe.paywithchallenge.domain.sequence.SequenceValidator
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class SequenceValidatorTest {

    lateinit var sequence: ArrayList<ButtonValue>
    lateinit var validator: SequenceValidator

    @Before
    fun `generate initial values`() {
        sequence = arrayListOf(
            ButtonValue.BUTTON_A,
            ButtonValue.BUTTON_A,
            ButtonValue.BUTTON_C
        )

        validator = SequenceValidator(sequence)
    }

    @Test
    fun `has the value in a different position`() {
        val result = validator.validate(2, ButtonValue.BUTTON_A)
        Assert.assertEquals(result, LEDStatus.ORANGE)
    }

    @Test
    fun `has found the value`() {
        val result = validator.validate(2, ButtonValue.BUTTON_C)
        Assert.assertEquals(result, LEDStatus.GREEN)
    }

    @Test
    fun `wrong value`() {
        val result = validator.validate(2, ButtonValue.BUTTON_B)
        Assert.assertEquals(result, LEDStatus.RED)
    }

    @Test
    fun `check index increment`() {
        validator.run {
            validate(1, ButtonValue.BUTTON_A)
            validate(1, ButtonValue.BUTTON_A)
        }

        Assert.assertEquals(validator.index, 2)
    }

    @Test
    fun `index is less than 3`() {
        validator.run {
            validate(2, ButtonValue.BUTTON_C)
            validate(2, ButtonValue.BUTTON_C)
            validate(2, ButtonValue.BUTTON_C)
            validate(2, ButtonValue.BUTTON_C)
        }

        Assert.assertFalse(validator.index >= 3)
    }

    @Test
    fun `avoid more than 3 attempts`() {
        validator.run {
            validate(2, ButtonValue.BUTTON_C)
            validate(2, ButtonValue.BUTTON_C)
            validate(2, ButtonValue.BUTTON_C)
            validate(2, ButtonValue.BUTTON_C)
        }

        Assert.assertFalse(validator.attempts.size > 3)
    }

    @Test
    fun `has successful sequence`() {
        validator.run {
            validate(0, ButtonValue.BUTTON_A)
            validate(1, ButtonValue.BUTTON_A)
            validate(2, ButtonValue.BUTTON_C)
        }

        Assert.assertTrue(validator.hasSuccess())
    }
}