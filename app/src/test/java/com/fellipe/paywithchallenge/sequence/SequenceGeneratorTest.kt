package com.fellipe.paywithchallenge.sequence

import com.fellipe.paywithchallenge.data.model.sequence.ButtonValue
import com.fellipe.paywithchallenge.domain.sequence.SequenceGenerator
import org.junit.Assert
import org.junit.Test

class SequenceGeneratorTest {

    @Test
    fun `generate 3 values as default`() {
        val values = SequenceGenerator.generateRandomSequence()
        Assert.assertEquals(values.size, 3)
    }

    @Test
    fun `random index is always less than 3`() {
        val randomIndex = SequenceGenerator.getRandomIndex()
        Assert.assertFalse(randomIndex >= 3)
    }

    @Test
    fun `converting index to button value`() {
        val buttonValue = SequenceGenerator.convertToButtonValue(0)
        Assert.assertEquals(buttonValue, ButtonValue.BUTTON_A)
    }
}