package com.fellipe.paywithchallenge

import android.app.Application
import com.fellipe.paywithchallenge.domain.di.resourceConverterModule
import com.fellipe.paywithchallenge.domain.di.resourceProviderModule
import com.fellipe.paywithchallenge.domain.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.module.Module

class BaseApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        initDependencyInjection()
    }

    private fun initDependencyInjection() {
        startKoin {
            androidContext(this@BaseApplication)
            modules(
                arrayListOf<Module>(
                    resourceProviderModule,
                    resourceConverterModule,
                    viewModelModule
                )
            )
        }
    }
}