package com.fellipe.paywithchallenge.presenter.main.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fellipe.paywithchallenge.BR
import com.fellipe.paywithchallenge.R
import com.fellipe.paywithchallenge.databinding.ActivityMainBinding
import com.fellipe.paywithchallenge.domain.core.bindingContentView
import com.fellipe.paywithchallenge.domain.core.goToActivityWithAnimation
import com.fellipe.paywithchallenge.domain.core.observe
import com.fellipe.paywithchallenge.domain.sequence.SequenceGenerator
import com.fellipe.paywithchallenge.domain.sequence.SequenceValidator
import com.fellipe.paywithchallenge.presenter.main.viewmodel.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class MainActivity: AppCompatActivity() {

    private val mainViewModel: MainViewModel by viewModel {
        parametersOf(
            SequenceValidator(
                SequenceGenerator.generateRandomSequence()
            )
        )
    }

    lateinit var binding: ActivityMainBinding

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = bindingContentView<ActivityMainBinding>(R.layout.activity_main).apply {
            setVariable(BR.viewModel, mainViewModel)
        }

        mainViewModel.action.subscribe(::subscribeActions)
    }

    private fun newAttempt() {
        goToActivityWithAnimation(true, MainActivity::class.java, R.anim.slide_in_right, R.anim.slide_out_left)
    }

    private fun subscribeActions(action: MainViewModel.SequenceAttemptAction) {
        when (action) {
            MainViewModel.SequenceAttemptAction.NextAttempt -> newAttempt()
            MainViewModel.SequenceAttemptAction.Close -> finish()
        }
    }
}