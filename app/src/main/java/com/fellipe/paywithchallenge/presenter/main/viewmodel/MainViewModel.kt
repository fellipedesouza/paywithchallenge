package com.fellipe.paywithchallenge.presenter.main.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.fellipe.paywithchallenge.data.led.LEDStatus
import com.fellipe.paywithchallenge.data.model.sequence.ButtonValue
import com.fellipe.paywithchallenge.domain.led.LEDResourceConverter
import com.fellipe.paywithchallenge.domain.sequence.SequenceValidator
import io.reactivex.subjects.PublishSubject

class MainViewModel(
    private val validator: SequenceValidator,
    private val resourceConverter: LEDResourceConverter
): ViewModel() {

    val color1 = ObservableField<Int>()
    val color2 = ObservableField<Int>()
    val color3 = ObservableField<Int>()

    val action = PublishSubject.create<SequenceAttemptAction>()
    val hasSuccess = ObservableBoolean(false)

    init {
        color1.set(resourceConverter.getColorResourceFromStatus(LEDStatus.OFF))
        color2.set(resourceConverter.getColorResourceFromStatus(LEDStatus.OFF))
        color3.set(resourceConverter.getColorResourceFromStatus(LEDStatus.OFF))
    }

    fun onClick(buttonValue: ButtonValue) {
        val status = validator.validate(value = buttonValue)
        val color = resourceConverter.getColorResourceFromStatus(status)

        updateColors(color)
        checkSuccess()
    }

    fun retry() {
        action.onNext(SequenceAttemptAction.NextAttempt)
    }

    fun onClose() {
        action.onNext(SequenceAttemptAction.Close)
    }

    private fun updateColors(newColor: Int) {
        color1.set(color2.get())
        color2.set(color3.get())
        color3.set(newColor)
    }

    private fun checkSuccess() {
        hasSuccess.set(validator.hasSuccess())
    }

    sealed class SequenceAttemptAction {
        object Close: SequenceAttemptAction()
        object NextAttempt: SequenceAttemptAction()
    }
}