package com.fellipe.paywithchallenge.data.led

enum class LEDStatus {
    RED,
    GREEN,
    ORANGE,
    OFF
}