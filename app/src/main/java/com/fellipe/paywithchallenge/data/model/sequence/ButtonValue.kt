package com.fellipe.paywithchallenge.data.model.sequence

enum class ButtonValue {
    BUTTON_A,
    BUTTON_B,
    BUTTON_C
}