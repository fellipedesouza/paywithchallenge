package com.fellipe.paywithchallenge.domain.core

import android.content.Context
import androidx.annotation.ColorRes
import androidx.annotation.IntegerRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat

class ResourceProvider constructor(private val context: Context) {

    fun getColor(@ColorRes resourceId: Int): Int {
        return ContextCompat.getColor(context, resourceId)
    }

    fun getInteger(@IntegerRes resourceId: Int): Int {
        return context.resources.getInteger(resourceId)
    }

    fun getString(
        @StringRes resourceId: Int,
        vararg formatArgs: Any?
    ): String {
        return context.getString(resourceId, *formatArgs)
    }
}