package com.fellipe.paywithchallenge.domain.core

import android.graphics.drawable.GradientDrawable
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter

object BindingAdapterUtils {

    @JvmStatic
    @BindingAdapter("setViewColor")
    fun setViewColor(view: View, color: Int) {
        val backgroundColor = view.background

        if (backgroundColor is GradientDrawable && color != 0) {
            backgroundColor.setColor(color)
        }
    }
}