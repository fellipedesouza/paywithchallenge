package com.fellipe.paywithchallenge.domain.di

import com.fellipe.paywithchallenge.domain.core.ResourceProvider
import com.fellipe.paywithchallenge.domain.led.LEDResourceConverter
import com.fellipe.paywithchallenge.domain.sequence.SequenceValidator
import com.fellipe.paywithchallenge.presenter.main.viewmodel.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val resourceProviderModule = module(override = true) {
    single<ResourceProvider> {
        ResourceProvider(
            context = get()
        )
    }
}

val resourceConverterModule = module(override = true) {
    single<LEDResourceConverter> {
        LEDResourceConverter(
            resourceProvider = get()
        )
    }
}

val viewModelModule = module(override = true) {
    viewModel { (sequenceValidator: SequenceValidator) -> MainViewModel(validator = sequenceValidator, resourceConverter = get()) }
}