package com.fellipe.paywithchallenge.domain.sequence

import com.fellipe.paywithchallenge.data.model.sequence.ButtonValue

object SequenceGenerator {

    fun generateRandomSequence(limit: Int = 3): ArrayList<ButtonValue> {
        val values = arrayListOf<ButtonValue>()
        for (i in 1..limit) {
            values.add(convertToButtonValue(getRandomIndex()))
        }

        return values
    }

    fun getRandomIndex(end: Int = 3, start: Int = 0) =
        (Math.random() * (end - start)).toInt() + start

    fun convertToButtonValue(index: Int) = ButtonValue.values()[index]
}