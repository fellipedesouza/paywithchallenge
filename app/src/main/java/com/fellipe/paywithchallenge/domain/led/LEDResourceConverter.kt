package com.fellipe.paywithchallenge.domain.led

import com.fellipe.paywithchallenge.R
import com.fellipe.paywithchallenge.data.led.LEDStatus
import com.fellipe.paywithchallenge.domain.core.ResourceProvider

class LEDResourceConverter(private val resourceProvider: ResourceProvider) {

    fun getColorResourceFromStatus(status: LEDStatus): Int {
        return when (status) {
            LEDStatus.GREEN -> resourceProvider.getColor(R.color.green)
            LEDStatus.ORANGE -> resourceProvider.getColor(R.color.orange)
            LEDStatus.RED -> resourceProvider.getColor(R.color.red)
            LEDStatus.OFF -> resourceProvider.getColor(R.color.gray)
        }
    }
}