package com.fellipe.paywithchallenge.domain.core

import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.AnimRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

fun <T> AppCompatActivity.bindingContentView(layout: Int): T {
    return DataBindingUtil.setContentView<ViewDataBinding>(this, layout)
        .also {
            it.lifecycleOwner = this
        } as T
}

fun AppCompatActivity.goToActivityWithAnimation(hasToFinish: Boolean, targetActivity: Class<*>, @AnimRes enterAnim: Int, @AnimRes exitAnim: Int) {
    startActivity(Intent(this, targetActivity))
    onFinish(hasToFinish)
    overridePendingTransition(enterAnim, exitAnim)
}

private fun AppCompatActivity.onFinish(hasToFinish: Boolean) {
    if (hasToFinish) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition()
        } else {
            finish()
        }
    }
}

fun <T : Any, L : LiveData<T>> LifecycleOwner.observe(liveData: L, expression: (T?) -> Unit) {
    liveData.observe(this, Observer(expression))
}