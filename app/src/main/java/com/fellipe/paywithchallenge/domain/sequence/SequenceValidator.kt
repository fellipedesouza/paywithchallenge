package com.fellipe.paywithchallenge.domain.sequence

import com.fellipe.paywithchallenge.data.led.LEDStatus
import com.fellipe.paywithchallenge.data.model.sequence.ButtonValue

class SequenceValidator(val sequence: ArrayList<ButtonValue>) {

    var index = 0
    val attempts = ArrayList<LEDStatus>()

    fun validate(position: Int = index, value: ButtonValue): LEDStatus {
        val status = when {
            value !in sequence -> LEDStatus.RED
            sequence[position] == value -> LEDStatus.GREEN
            sequence[position] != value -> LEDStatus.ORANGE
            else -> LEDStatus.OFF
        }

        incrementIndex(status)
        updateAttempts(status)

        return status
    }

    private fun incrementIndex(status: LEDStatus) {
        if (index < 2 && status == LEDStatus.GREEN)
            index += 1
        else
            clearValues()
    }

    private fun clearValues() {
        this.index = 0
    }

    private fun updateAttempts(status: LEDStatus) {
        attempts.add(0, status)

        if (attempts.size > 3)
            attempts.removeAt(attempts.size - 1)
    }

    fun hasSuccess() = attempts.size == 3 && attempts.all { status -> status == LEDStatus.GREEN }
}